#Alejandro Martínez - Ejercicio #1
import os
 
def cls():
    os.system('cls' if os.name=='nt' else 'clear')
 
def end():
    while(True):
        entrada = input("¿Desea salir? (y/n): ")
        if(entrada == "y" or entrada == "Y"):
            return True
        elif(entrada == "n" or entrada == "N"):
            return False
        else:
            print("Opción ingresada no es válida")
 
def calc_rut(rut):
    suma        = 0
    multiplicar = 2
    for i in rut [::-1]:
        suma += int (i) * multiplicar
        multiplicar += 1
        if multiplicar == 8:
            multiplicar = 2
    final = suma % 11
    final = 11 - final
    if final == 11:
        return 0
    elif final == 10:
        return "K"
    else:
        return final
 
salir  = False
while (not salir):
    try:
        cls()
        print("Bienvenido,\nFavor ingresar un RUT (Sin digito verificador):\n")
        rut     = input("RUT: ")
    except Exception as error:
        print("==== ERROR DEBUG: %s ====" % (error))
    except KeyboardInterrupt:
        print("\n")
        if(end() == True):
            salir = True
    finally:
        print("""
El digito verificador del RUT %s es %s.
RUT con D.Verificador: %s-%s
       """ % (rut, calc_rut(rut), rut, calc_rut(rut)))
        if(end() == True):
            salir = True
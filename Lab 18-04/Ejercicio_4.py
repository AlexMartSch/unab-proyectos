# Alejandro Martínez - Ejercicio 4

import os
 
def cls():
    os.system('cls' if os.name=='nt' else 'clear')
 
def end():
    while(True):
        entrada = input("¿Desea salir? (y/n): ")
        if(entrada == "y" or entrada == "Y"):
            return True
        elif(entrada == "n" or entrada == "N"):
            return False
        else:
            print("Opcion ingresada no es valida")
 
def calc_puntaje(lista, dificultad):
    lista.remove(min(lista))
    lista.remove(max(lista))
    return float(sum(lista))*0.6*float(dificultad)
   
 
def check(ingesta):
    if (1.0 <= ingresta <= 10.0 and round(ingresta,1) == ingresta):
        return True
    return False
 
salir  = 0
puntos = []
while (not salir):
    try:
        cls()
        print("Bienvenido,\nFavor ingresar el nombre del competidor y posteriormente los puntajes de cada juez:\n")
        nombre      = input("Nombre del competidor: ")
        dificultad  = float(input("Grado de dificultad: "))
        print()
        for i in range(1, 8):
            temp = False
            while (not temp):
                ingesta = float(input("Juez %d: " % (i)))
                if(check(ingesta) == True):
                    puntos.append(ingesta)
                    temp = True
                else:
                  print("Tienes que ingresar un rango entre 1 y 10 (o 1.0 y 10.0)")
    except Exception as error:
        print("==== ERROR DEBUG: %s ====" % (error))
    except KeyboardInterrupt:
        print("\n")
        if(end() == True):
            salir = True
    finally:
        final = calc_puntaje(puntos, dificultad)
        print()
        print(">> El puntaje total es: %s" % (final))
        print()
        if(end() == True):
            salir = True
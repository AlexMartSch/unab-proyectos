# Alejandro Martínez - Ejercicio 3
import os

def cls():
    os.system('cls' if os.name=='nt' else 'clear')
 
def end():
    while(True):
        entrada = input("¿Desea salir? (y/n): ")
        if(entrada == "y" or entrada == "Y"):
            return True
        elif(entrada == "n" or entrada == "N"):
            return False
        else:
            print("Opcion ingresada no es valida")

def CalcularTriangulo(a1, a2, a3):
    if((a1==90 and a2==90) or (a2==90 and a3==90) or (a1==90 and a3==90)):
        return "Error de calculo, los angulos solo deben sumar 180 grados."
    if (a1==90) or (a2==90) or (a3==90):
        return "Rectángulo"
    elif (a1<90) and (a2<90) and (a3<90):
        return "Acutángulo"
    elif (a1>90) or (a2>90) or (a3>90):
        return "Obstusángulo"

salir  = 0
while (not salir):
    try:
        cls()
        print("Bienvenido,\nFavor ingresar el nombre del competidor y posteriormente los puntajes de cada juez:\n")
        angulo1 = int(input("Ingrese el primer angulo del triangulo: "))
        angulo2 = int(input("Ingrese el otro angulo del triangulo: "))
        angulo3 = 180-(angulo1+angulo2)
        print()
    except Exception as error:
        print("==== ERROR DEBUG: %s ====" % (error))
    except KeyboardInterrupt:
        print("\n")
        if(end() == True):
            salir = True
    finally:
        final = CalcularTriangulo(angulo1, angulo2, angulo3)
        print()
        print(">> El Triangulo es: %s" % (final))
        print()
        if(end() == True):
            salir = True

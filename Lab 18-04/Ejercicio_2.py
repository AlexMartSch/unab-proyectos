# Alejandro Martínez - Ejercicio #2
import os
 
def cls():
    os.system('cls' if os.name=='nt' else 'clear')
 
def end():
    while(True):
        entrada = input("¿Desea salir? (y/n): ")
        if(entrada == "y" or entrada == "Y"):
            return True
        elif(entrada == "n" or entrada == "N"):
            return False
        else:
            print("Opcion ingresada no es valida")

def MuestramePrecio(piso, numero):
    dpto = int(str(piso)+str(numero)) 
    if(len(str(dpto))==3):
      dpto = int(str(0)+str(dpto))
    
    pisomaximo=2007
    pisominimo=100

    piso_intermedio_min = 200
    piso_intermedio_max_3 = 907
    piso_intermedio_max = 1907
    
    intermedio_min=200
    intermedio_max=1900
    
    valor_primer_piso = 100
    valor_ultimo_piso = 400
    valor_intermedio = 250
    
    valor_incremento_mar_intermedio = 38
    valor_disminuir_cerro_intermedio = 250
    
    valor_incremento_mar_ultimo_piso = 60
    valor_disminuir_cerro_ultimo_piso = 80

    if(not (dpto<=pisomaximo and dpto>=pisominimo)):
        return "¡Ese depto no existe!"
    
    if(dpto<1000):
      if(dpto<intermedio_min):
        dpto = str(dpto)
        if((dpto[2] >= "0") and (dpto[2] <= "7")):
          return str("$"+str(int(valor_primer_piso)))

      if(dpto>=intermedio_min and dpto<=piso_intermedio_max_3):
        dpto = str(dpto)
        if((dpto[2] == "0") or (dpto[2] == "4")):
          return str("$"+str(int(valor_intermedio-valor_disminuir_cerro_intermedio)))
        if((dpto[2] == "3") or (dpto[2] == "7")):
          return str("$"+str(int(valor_intermedio+valor_incremento_mar_intermedio)))
        if((dpto[2] >= "1") and (dpto[2] <= "6")):
          return str("$"+str(int(valor_intermedio)))
    
    if(dpto>=1000):
      if(dpto>=2000):
        dpto = str(dpto)
        if((dpto[3] >= "0") and (dpto[3] <= "7")):
          return str("$"+str(int(valor_ultimo_piso)))

      if(dpto>piso_intermedio_max_3 and dpto<=pisomaximo):
        dpto = str(dpto)
        if((dpto[3] == "0") or (dpto[3] == "4")):
          return str("$"+str(int(valor_ultimo_piso-valor_disminuir_cerro_ultimo_piso)))
        if((dpto[3] == "3") or (dpto[3] == "7")):
          return str("$"+str(int(valor_ultimo_piso+valor_incremento_mar_ultimo_piso)))
        if((dpto[3] >= "1") and (dpto[3] <= "6")):
          return str("$"+str(int(valor_ultimo_piso)))

    return "¡Hey! Ese depto no existe."

salir  = 0
while (not salir):
    try:
        cls()
        print("Bienvenido,\nFavor ingresar el numero de departamento, entre el 100 y 2007\nConsidere que el edificio tiene 20 pisos y 8 departamentos por cada piso.\n")
        iddptop      = input("Ingrese el numero del departamento: [0000]: ")
        piso = iddptop[0:2]
        numero = iddptop[2:4]

    except Exception as error:
        print("==== ERROR DEBUG: %s ====" % (error))
    except KeyboardInterrupt:
        print("\n")
        if(end() == True):
            salir = True
    finally:
        final = MuestramePrecio(piso, numero)
        print()
        print(">> EL Precio es de: %s" % (final))
        print()
        if(end() == True):
            salir = True
        